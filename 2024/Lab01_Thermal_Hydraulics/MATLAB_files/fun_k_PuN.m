%PuN fuel Thermal Conductivity
function [k] = fun_k_PuN(T)

%k = (1./(0.042 + 2.71*(10.^-4).*T)) + 6.9*(10.^-11)*(T.^3);  %Fast Spectrum Reactors

k = 3.3.*T.^(0.19); %Wallenius

end