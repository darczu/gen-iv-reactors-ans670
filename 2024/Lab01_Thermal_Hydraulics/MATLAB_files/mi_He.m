%Dynamic Viscosity for Helium - based on MELCOR Power Lw Fit   Dynamic Visc. = [kg/s/m] = [Pa*s]
function [mi_He] = mi_He(T)
mi_He =    (3.674).*T.^(0.7)   %[kg/m/s]       %[Pa*s] =        
end