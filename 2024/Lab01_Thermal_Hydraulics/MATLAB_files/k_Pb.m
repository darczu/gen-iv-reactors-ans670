%Lead Thermal Conductivity [W/m/K]
function [k_Pb] = k_Pb(T_Pb)
k_Pb = 9.2 + 0.011.*T_Pb;    %Wallenius
end