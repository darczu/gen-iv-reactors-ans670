%Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
function [mi_Na] = mi_Na(T_Na)
mi_Na = exp(-6.4406 + (556.835./T_Na))./(T_Na.^0.3958);     %[kg/m/s]       %[Pa*s] =        
end