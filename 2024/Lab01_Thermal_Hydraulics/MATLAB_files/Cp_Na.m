%Na c_p(T) - specific heat capacity

function [Cp_Na] = Cp_Na(T_Na)

%y11 = 1658.2 - 0.8479.*L11 + 4.4541.*(10.^-4).*(L11.^2) - 2.9926.*(10.^6).*(L11.^-2);
Cp_Na = 1658.2 - 0.8479.*T_Na + 4.4541.*(10.^-4).*(T_Na.^2) - 2.9926.*(10.^6).*(T_Na.^-2); %Wallenius
end