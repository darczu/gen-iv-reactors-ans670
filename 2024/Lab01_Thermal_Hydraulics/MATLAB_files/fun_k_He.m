%Helium Thermal Conductivity
% P - Pressure [Pa]
function [k] = fun_k_He(T)

k = 0.0574 + (3.45e-4).*T - (4.05e-8).*(T.^2); %Wallenius Chapter 6

%Alternative correlations
%k = (2.692e-3).*((1.0 + (1.123e-8).*P).*T.^(0.71)).*(1.0-(2e-9).*P);   %Base on MECLOR Refernce Manual
%k = k = (2.685e-3).*T.^(0.71);   %Alternative Power law fit for MELCOR applications


end