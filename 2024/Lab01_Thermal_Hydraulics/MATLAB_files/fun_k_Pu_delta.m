%Metallic Plutonium DELTA phase Thermal Conductivity
function [k] = fun_k_Pu_delta(T)

% RANGE:   593 < T < 736K
if ()
   k = 3.225 + 0.0296.*T; %Wallenius Chapter 6
else
	error('Temperature outside application range range:  593K < T < 736K');
end