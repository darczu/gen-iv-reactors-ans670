%Sodium density [kg/m3]
function [rho_Na] = rho_Na(T_Na)
 rho_Na = 1011.8 - 0.22054.*T_Na - 1.9226.*(10.^-5).*(T_Na.^2) + 5.6371.*(10.^-9).*(T_Na.^3); %Fast Spec Reactor
end