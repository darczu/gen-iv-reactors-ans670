%Reynolds number calculation
% G - mass flux m/A = kg/s/m2
% Dh - hydrualic diam
% mi - dynamics viscosity

function [Re] = fun_Re(m,A,Dh,mi)

G = m./A;

Re = G.*Dh./mi;


end