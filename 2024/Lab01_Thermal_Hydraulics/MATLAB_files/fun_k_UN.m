%UN fuel Thermal Conductivity
function [k] = fun_k_UN(T)

k = 1.864.*T.^(0.361);  %Wallenius

end