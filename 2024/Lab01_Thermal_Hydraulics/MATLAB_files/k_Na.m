%Sodium thermal conductivity [W/m/K]
function [k_Na] = k_Na(T_Na)
 k_Na = 93.0 - 0.0581.*(T_Na-273) + 1.173.*(10.^-5).*((T_Na-273).^2);  %Fast Spec Reactors
 %k_Na = 109.7 - 0.0645.*T_Na + 1.173.*(1e-5).*(T_Na.^2);  %Wallenius
 
end