%Metallic Plutonium EPSILON phase Thermal Conductivity
function [k] = fun_k_Pu_epsilon(T)

% RANGE:   756 < T < 913K
if ()
    k = 9.507 + 0.0184.*T; %Wallenius Chapter 6
else
	error('Temperature outside application range range:  756K < T < 913K');
end