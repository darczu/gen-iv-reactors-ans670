%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update 15-04-2024, Class 1, Gen IV ANS670 Laboratory - MATLAB Thermal-Hydraulics 1D Lumped-Parameter code to Calc. SFR and LFR
% Developed by Piotr Darnowski, piotr.darnowski@pw.edu.pl, Warsaw University of Technology, Institute of Heat Engineering, Faculty of Power and Aeronautical Engineering
% Apply Isolated Subchannel Model (ISM)
% 1 - Sodium Cooled;  2 - Lead Cooled
% '%% '  two percent and space defines code section
% '%'    one percent - comment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear; clc;					%close all figures; clear workspace; clean workspace
%% Input Data
H_1  = 1.0;      H_2  =  1.0;			%Active Core Length, [m]   %Semicolon ';' - do not show variable in the workspace. It allows to define multiple variable in single line
He_1 = H_1 + 2* 0.35*H_1;  				%Extrapolated core height, [m]
He_2 = H_1 + 2* 0.5*H_2;   		 		%Extrapolated core height, [m]
%% 
Dclad_1 = 8.2e-3;  Dclad_2 = 10.5e-3;	%Fuel pin outer diameter, [m] - cladding outer diameter
tclad_1 = ;   
tclad_2  = ;    						%Cladding Thickness, [m]   
Diclad_1 = Dclad_1 - 2*tclad_1;			%Inner cladding diameter, [m]
Diclad_2 = Dclad_2 - 2*tclad_2;		
	
%% Input Data: Assume fuel cladding contact
Dfuel_1 = Diclad_1;  	
Dfuel_2 = Diclad_2;		
%Fuel pin diameter, [m]
PD_1    = ;      		
PD_2    = ;     	 	
%Pitch-to-diameter ratio, [-] %Pitch = (P/D) * D, [m]
pitch_1 = ;  			
pitch_2 = ;      		
%Av. Coolant velocity. [m/s]
v_1    = ;   			
v_2     = ; 			
%Peak linear power, [W/m]
q0_1   = ;   			
q0_2    = ;		   		
%Inlet Temperature, [K]
Tin_1  = ;   			
Tin_2   = ;
%Cladding thermal conductivity [W/m/K]
kc_1   = ; 				
kc_2 	= ;				
%Gas gap heat transfer coefficient [W/m2/K]
h_gap = ;

%% Input Data: Linear Power [W/m] distribution - Chopped cosine profile assumed
z_11 = linspace(-H_1/2,H_1/2,50);	 			%location vector, [m], with 50 values, equally spaced, beetwen -H_1/2 to H_2/2		    
q_1  = ;										%vector with linear power values for z_11
z_22 = linspace(-H_2/2,H_2/2,50);
q_2  = ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plot Linear Power Profile - an example how to prepare simple plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(1);								% define figure
plot(z_11,q_1,'--m',z_22,q_2,'c-');		% plot with two data sets plot(X,Y,x,y) 
legend('power Na', 'power Pb');			% legend
ylabel('Temperature, K','fontsize',16); % y label
xlabel('Distance, m','fontsize',16);  	% x label
grid on;								% add grid


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 1 - Coolant temperature distribution calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.1 - Calc. average linear power density and axial peaking factor.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.2, 1.3 - Calc. flow area, wetted perimeter (in this case it is also equal to the heated perimeter).
%Calc. channel geometry for ISM % Hint: Triangle area = sqrt(3)/4*P^2



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.4 - Calc. mass flow using inlet density and temperature.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 1.5 - Calc.  axial  temperature  profile  for  bulk  flow  of  coolant,  
% assume  constant  specific  heat capacity. Find outlet coolant temperature. Plot results.



% Plot
% figure(2)
% plot(z_11,T_1,z_22,T_2);
% legend('Na','Pb');
% ylabel('Temperature, K','fontsize',16)
% xlabel('Distance, m','fontsize',16)
% grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 2 - Cladding temperatures calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.1 - Calc. dynamic viscosity distribution in the channel. 




% Plot
%plot(z_11,mi_1,z_22,mi_2)
%legend('Na','Pb')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.2 -  Calc. Reynolds number distribution in the channel. You can use mass flux


%plot(z_11, Re_1,'o', z_22, Re_2)
%legend('Na','Pb');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.3 -  Calc. specific heat capacity distribution. 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.4 -  Calc. coolant heat conductivity distribution.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.5 -  Calc. Prandtl number distribution.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.6 -  Calc. Peclet number distribution.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.7 - Calc. Heat Transfer Coefficient using Nusselt Number. Select proper correlation  (use Lecture 7).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.8 - Calc. the cladding surface outer temperature. Find maximum value.


% Plot
%plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2);
%legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out');
%ylabel('Temperature, K','fontsize',16)
%xlabel('Distance, m','fontsize',16)
%grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 2.9 - Calc. the cladding  surface  inner   temperature. Find maximum value and plot results for both cladding temperatures.




% Plot
% figure(3)
% plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2);
% legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'Pb clad in');
% ylabel('Temperature, K','fontsize',16)
% xlabel('Distance, m','fontsize',16)
% grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 3 - Fuel temperature calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 3.1 -  Calc. fuel outer surface temperature distribution across the channel. Find maximum value. 



% Plot
% figure(4)
% plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2);
% plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:');
% legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out');
% ylabel('Temperature, K','fontsize',16)
% xlabel('Distance, m','fontsize',16)
% grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 3.2 - Calc. fuel centerline temperature distribution with iterative process presented in Appendix 1. 
% Find max temperature value and plot fuel temperatures.



%Fuel centreline temperature profile
%first temperature guess
%  Tfc_1 = ones(numel(q_1),1).*2500.0;
%  Tfc_2 = ones(numel(q_2),1).*2500.0;
%  kf_1 = zeros(numel(q_1),1);
%  kf_2 = zeros(numel(q_2),1);
 
% Iterative procedure :




% Plot
% figure(5)
% plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2, z_11, Tfc_1, z_22, Tfc_2 );
% plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:', z_11, Tfc_1, 'r.-', z_22, Tfc_2,'.-b');
% legend('Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out', 'Na fuel center', 'Pb fuel center');
% ylabel('Temperature, K','fontsize',16)
% xlabel('Distance, m','fontsize',16)
% grid on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Task 4 - Pressure Drop calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.1 - Calculate friction pressure losses.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.2 - Losses due to the wire wrapper for SFR and spacer grids for LFR. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.3 - Elevation pressure drop.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.4 - Local losses at the assembly inlet and exit.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Task 4.5 - Calculate the total pressure drop across the core.













