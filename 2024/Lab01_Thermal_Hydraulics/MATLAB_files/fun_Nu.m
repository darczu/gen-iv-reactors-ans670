%Nusselt number correlations
% Pe - Peclet
% s - pitch-to-diameter
% option - correlation selection
function [Nu] = fun_Nu(Pe,option,s)

if(strcmp(option,'sqr'))
	Nu = 0.48 + 0.0133.*Pe.^(0.70);  %tight square lattice

elseif(strcmp(option,'tri'))
	Nu = 0.25 + 6.2.*s + (0.32.*s-0.07).*(Pe.^(0.8-0.024.*s)); 
	%triangular lattice by Graber & Rieger

elseif(strcmp(option,'pure'))
	Nu = 5.0 + 0.025.*Pe.^(0.8);
	
elseif(strcmp(option,'norm'))	
	Nu = 3.3 + 0.014.*Pe.^(0.8);
	
else 
  error('Option not available !!! ');
end