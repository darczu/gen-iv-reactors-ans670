%Pure Uranium Thermal Conductivity
function [k] = fun_k_U(T)

k = 21.7+(1.59e-2).*T + (5.91e-6).*(T.^2); %Wallenius Chapter 6

end