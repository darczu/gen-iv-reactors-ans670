%Americium Metal Theoretical Thermal Conductivity
function [k] = fun_k_Am(T)

k = 4.118 + 0.018.*T + (5.35e-6).*(T.^2); %Wallenius Chapter 6

end