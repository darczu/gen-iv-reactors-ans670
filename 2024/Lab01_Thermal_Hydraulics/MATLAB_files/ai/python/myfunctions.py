import numpy as np

# Dynamic Viscosity for Helium - based on MELCOR Power Lw Fit
# Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_He(T):
    return 3.674 * T**0.7

# Lead Thermal Conductivity [W/m/K]
def k_Pb(T_Pb):
    return 9.2 + 0.011 * T_Pb

# Sodium thermal conductivity [W/m/K]
def k_Na(T_Na):
    return 93.0 - 0.0581 * (T_Na-273) + 1.173 * 10**-5 * (T_Na-273)**2

# Nusselt number correlations
def fun_Nu(Pe, option, s):
    if option == 'sqr':
        return 0.48 + 0.0133 * Pe**0.70
    elif option == 'tri':
        return 0.25 + 6.2 * s + (0.32 * s - 0.07) * Pe**(0.8 - 0.024 * s)
    elif option == 'pure':
        return 5.0 + 0.025 * Pe**0.8
    elif option == 'norm':
        return 3.3 + 0.014 * Pe**0.8
    else:
        raise ValueError('Option not available !!!')

# MOX fuel Thermal Conductivity
def fun_k_MOX(T):
    return 1 / (0.042 + 2.71 * 10**-4 * T) + 6.9 * 10**-11 * T**3

# Pb c_p(T) - specific heat capacity
def Cp_Pb(T_Pb):
    return 175.1 - 4.961 * 10**-2 * T_Pb + 1.985 * 10**-5 * T_Pb**2 - 2.099 * 10**-9 * T_Pb**3 - 1.524 * 10**6 * T_Pb**-2

# Na c_p(T) - specific heat capacity
def Cp_Na(T_Na):
    return 1658.2 - 0.8479 * T_Na + 4.4541 * 10**-4 * T_Na**2 - 2.9926 * 10**6 * T_Na**-2

# Lead density [kg/m3]
def rho_Pb(T_Pb):
    return 11367 - 1.1944 * T_Pb

# Sodium density [kg/m3]
def rho_Na(T_Na):
    return 1011.8 - 0.22054 * T_Na - 1.9226 * 10**-5 * T_Na**2 + 5.6371 * 10**-9 * T_Na**3

# Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_Pb(T_Pb):
    return 4.55 * 10**-4 * np.exp(1069 / T_Pb)

# Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_Na(T_Na):
    return np.exp(-6.4406 + 556.835 / T_Na) / T_Na**0.3958