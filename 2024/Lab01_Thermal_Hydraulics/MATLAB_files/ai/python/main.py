# Piotr Darnowski 2024
# Python version of the code. Rewritten by AI for Python using Copilot AI coupled to Interne Explorer
# https://clouds.eos.ubc.ca/~phil/docs/problem_solving/07-Functions-and-Modules/07.05-Calling-Functions-from-Other-Files.html
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad
from scipy.optimize import fsolve
    
# Dynamic Viscosity for Helium - based on MELCOR Power Lw Fit
# Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_He(T):
    return 3.674 * T**0.7

# Lead Thermal Conductivity [W/m/K]
def k_Pb(T_Pb):
    return 9.2 + 0.011 * T_Pb

# Sodium thermal conductivity [W/m/K]
def k_Na(T_Na):
    return 93.0 - 0.0581 * (T_Na-273) + 1.173 * 10**-5 * (T_Na-273)**2

# Nusselt number correlations
def fun_Nu(Pe, option, s):
    if option == 'sqr':
        return 0.48 + 0.0133 * Pe**0.70
    elif option == 'tri':
        return 0.25 + 6.2 * s + (0.32 * s - 0.07) * Pe**(0.8 - 0.024 * s)
    elif option == 'pure':
        return 5.0 + 0.025 * Pe**0.8
    elif option == 'norm':
        return 3.3 + 0.014 * Pe**0.8
    else:
        raise ValueError('Option not available !!!')

# MOX fuel Thermal Conductivity
def fun_k_MOX(T):
    return 1 / (0.042 + 2.71 * 10**-4 * T) + 6.9 * 10**-11 * T**3

# Pb c_p(T) - specific heat capacity
def Cp_Pb(T_Pb):
    return 175.1 - 4.961 * 10**-2 * T_Pb + 1.985 * 10**-5 * T_Pb**2 - 2.099 * 10**-9 * T_Pb**3 - 1.524 * 10**6 * T_Pb**-2

# Na c_p(T) - specific heat capacity
def Cp_Na(T_Na):
    return 1658.2 - 0.8479 * T_Na + 4.4541 * 10**-4 * T_Na**2 - 2.9926 * 10**6 * T_Na**-2

# Lead density [kg/m3]
def rho_Pb(T_Pb):
    return 11367 - 1.1944 * T_Pb

# Sodium density [kg/m3]
def rho_Na(T_Na):
    return 1011.8 - 0.22054 * T_Na - 1.9226 * 10**-5 * T_Na**2 + 5.6371 * 10**-9 * T_Na**3

# Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_Pb(T_Pb):
    return 4.55 * 10**-4 * np.exp(1069 / T_Pb)

# Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
def mi_Na(T_Na):
    return np.exp(-6.4406 + 556.835 / T_Na) / T_Na**0.3958    
    
# MAIN CODE    

# Input Data
H_1  = 1.0
H_2  = 1.0
He_1 = H_1 + 2* 0.35*H_1
He_2 = H_1 + 2* 0.5*H_2

Dclad_1 = 8.2e-3
Dclad_2 = 10.5e-3
tclad_1 = 0.52e-3
tclad_2  = 0.6e-3
Diclad_1 = Dclad_1 - 2*tclad_1
Diclad_2 = Dclad_2 - 2*tclad_2

Dfuel_1 = Diclad_1
Dfuel_2 = Diclad_2
PD_1 =  1.15
PD_2    = 1.32
pitch_1 = PD_1*Dclad_1
pitch_2 =  PD_2*Dclad_2
v_1    = 6.7
v_2     = 1.8
q0_1   =  42000
q0_2    = 23200
Tin_1  = 400 +273
Tin_2   = 400 + 273
kc_1   = 20
kc_2 	= 28
h_gap = 6000

z_11 = np.linspace(-H_1/2,H_1/2,50)
q_1  = q0_1*np.cos(np.pi*z_11/He_1)
z_22 = np.linspace(-H_2/2,H_2/2,50)
q_2  = q0_2*np.cos(np.pi*z_22/He_2)

plt.figure(1)
plt.plot(z_11,q_1,'--m',z_22,q_2,'c-')
plt.legend(['power Na', 'power Pb'])
plt.ylabel('Linear Heat Rate, [W/m]')
plt.xlabel('Distance, m')
plt.ylim([0.0, np.inf])
plt.grid(True)

qav_1 = np.trapz(z_11,q_1)/H_1
qav_2 = np.trapz(z_22,q_2)/H_2
Fa_1 = q0_1/qav_1
Fa_2 = q0_2/qav_2

A_1 = (np.sqrt(3)/4)*(pitch_1**2) - (np.pi*Dclad_1**2)/8
A_2 = (pitch_2**2) - np.pi*(Dclad_2**2)/4

Ph_1 = 0.5*np.pi*Dclad_1
Dh_1 = 4*A_1/Ph_1

Ph_2 = np.pi*Dclad_2
Dh_2 = 4*A_2/Ph_2


rho_in1 = rho_Na(Tin_1)
rho_in2 = rho_Pb(Tin_2)

m_1 = v_1*rho_in1*A_1
m_2 = v_2*rho_in2*A_2





# Task 1.5 - Calc. axial temperature profile for bulk flow of coolant
cp_1 = Cp_Na(Tin_1)
cp_2 = Cp_Pb(Tin_2)

T_1 = Tin_1 + ((0.5 * q0_1 * He_1) / (np.pi * cp_1 * m_1)) * (np.sin(np.pi * z_11 / He_1) + np.sin(np.pi * H_1 / (2 * He_1)))
T_2 = Tin_2 + ((1.0 * q0_2 * He_2) / (np.pi * cp_2 * m_2)) * (np.sin(np.pi * z_22 / He_2) + np.sin(np.pi * H_2 / (2 * He_2)))

plt.figure(2)
plt.plot(z_11, T_1, z_22, T_2)
plt.legend(['Na', 'Pb'])
plt.ylabel('Temperature, K', fontsize=16)
plt.xlabel('Distance, m', fontsize=16)
plt.grid(True)

# Task 2 - Cladding temperatures calculation

# Task 2.1 - Calc. dynamic viscosity distribution in the channel
mi_1 = mi_Na(T_1)
mi_2 = mi_Pb(T_2)

plt.figure()
plt.plot(z_11, mi_1, z_22, mi_2)
plt.legend(['Na', 'Pb'])

# Task 2.2 - Calc. Reynolds number distribution in the channel
G_1 = m_1 / A_1
G_2 = m_2 / A_2
Re_1 = G_1 * Dh_1 / mi_1
Re_2 = G_2 * Dh_2 / mi_2

plt.figure()
plt.plot(z_11, Re_1, 'o', z_22, Re_2)
plt.legend(['Na', 'Pb'])

# Task 2.3 - Calc. specific heat capacity distribution
cp_1 = Cp_Na(T_1)
cp_2 = Cp_Pb(T_2)

# Task 2.4 - Calc. coolant heat conductivity distribution
k_1 = k_Na(T_1)
k_2 = k_Pb(T_2)

# Task 2.5 - Calc. Prandtl number distribution
Pr_1 = cp_1 * mi_1 / k_1
Pr_2 = cp_2 * mi_2 / k_2

# Task 2.6 - Calc. Peclet number distribution
Pe_1 = Re_1 * Pr_1
Pe_2 = Re_2 * Pr_2

plt.figure()
plt.plot(z_11, Pe_1, 'o', z_22, Pe_2)

# Task 2.7 - Calc. Heat Transfer Coefficient using Nusselt Number
Nu_1 = fun_Nu(Pe_1, 'tri', PD_1)
Nu_2 = fun_Nu(Pe_2, 'sqr', PD_2)

h_1 = k_1 * Nu_1 / Dh_1
h_2 = k_2 * Nu_2 / Dh_2

# Task 2.8 - Calc. the cladding surface outer temperature
Tco_1 = T_1 + q_1 / (h_1 * Dclad_1 * np.pi)
Tco_2 = T_2 + q_2 / (h_2 * Dclad_2 * np.pi)

plt.figure()
plt.plot(z_11, T_1, z_22, T_2, z_11, Tco_1, z_22, Tco_2)
plt.legend(['Na flow', 'Pb flow', 'Na clad out', 'Pb clad out'])
plt.ylabel('Temperature, K', fontsize=16)
plt.xlabel('Distance, m', fontsize=16)
plt.grid(True)



##########################################



# Task 2.9 - Calc. the cladding  surface  inner   temperature. Find maximum value and plot results for both cladding temperatures.
# Cladding inner temperature
Tci_1 =  Tco_1 + q_1/(2*np.pi*kc_1)*np.log(Dclad_1/Diclad_1)
Tci_2 =  Tco_2 + q_2/(2*np.pi*kc_2)*np.log(Dclad_2/Diclad_2)

plt.figure(3)
plt.plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2)
plt.legend(['Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'Pb clad in'])
plt.ylabel('Temperature, K',fontsize=16)
plt.xlabel('Distance, m',fontsize=16)
plt.grid(True)

# Task 3 - Fuel temperature calculation
# Task 3.1 -  Calc. fuel outer surface temperature distribution across the channel. Find maximum value. 
# Fuel outer surface temperature
Tfo_1 = Tci_1 + q_1/(np.pi*Dfuel_1*h_gap)
Tfo_2 = Tci_2 + q_2/(np.pi*Dfuel_2*h_gap)

plt.figure(4)
plt.plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2)
plt.plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:')
plt.legend(['Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out'])
plt.ylabel('Temperature, K',fontsize=16)
plt.xlabel('Distance, m',fontsize=16)
plt.grid(True)

# Task 3.2 - Calc. fuel centerline temperature distribution with iterative process presented in Appendix 1. 
# Find max temperature value and plot fuel temperatures.
# Fuel centreline temperature profile
# first temperature guess
Tfc_1 = np.ones_like(q_1)*2500.0
Tfc_2 = np.ones_like(q_2)*2500.0
kf_1 = np.zeros_like(q_1)
kf_2 = np.zeros_like(q_2)

# Iterative procedure :
for i in range(50):
    eps=1.0
    while eps>1e-5:  
        T_prev = Tfc_1[i]
        kf_1[i], _ = quad(fun_k_MOX,Tfo_1[i],Tfc_1[i])
        kf_1[i] = 1/(Tfc_1[i]-Tfo_1[i])*kf_1[i]
        Tfc_1[i] = Tfo_1[i] +  q_1[i]/(4*np.pi*kf_1[i])
        eps=abs((Tfc_1[i]-T_prev)/T_prev)

for i in range(50): 
    eps=1.0
    while eps>1e-5: 
        T_prev = Tfc_2[i]
        kf_2[i], _ = quad(fun_k_MOX,Tfo_2[i],Tfc_2[i])
        kf_2[i] = 1/(Tfc_2[i]-Tfo_2[i])*kf_2[i]
        Tfc_2[i] = Tfo_2[i] +  q_2[i]/(4*np.pi*kf_2[i])
        eps=abs((Tfc_2[i]-T_prev)/T_prev)

plt.figure(5)
plt.plot(z_11,T_1,z_22,T_2, z_11, Tco_1, z_22, Tco_2,z_11,Tci_1,z_22,Tci_2, z_11, Tfo_1, z_22, Tfo_2, z_11, Tfc_1, z_22, Tfc_2 )
plt.plot(z_11,T_1,'r-',z_22,T_2,'b-', z_11, Tco_1,'r--', z_22, Tco_2,'b--',z_11,Tci_1,'ro',z_22,Tci_2,'bo', z_11, Tfo_1,'r:', z_22, Tfo_2,'b:', z_11, Tfc_1, 'r.-', z_22, Tfc_2,'.-b')
plt.legend(['Na flow','Pb flow', 'Na clad out', 'Pb clad out','Na clad in', 'PB clad out','Na fuel out', 'Pb fuel out', 'Na fuel center', 'Pb fuel center'])
plt.ylabel('Temperature, K',fontsize=16)
plt.xlabel('Distance, m',fontsize=16)
plt.grid(True)

# Task 4 - Pressure Drop calculation
# Task 4.1 - Calculate friction pressure losses.
rho_1 = np.mean(rho_Na(T_1))  #mean density of sodium
rho_2 = np.mean(rho_Pb(T_2))  #mean densty of lead

# Fanning friction factor coefficient
Cf_1 = 0.25*(0.96*(PD_1+0.63))/((1.82*np.log10(np.mean(Re_1))-1.64)**2)     # Sodium, For triangular lattice
Cf_2 = 0.0791/(np.mean(Re_2)**0.25)                                         # Lead, Blassius for square lattice

dp_fric_1 = 4*Cf_1*H_1*(G_1**2)/(Dh_1*2*rho_1)  #sodium pressure drop
dp_fric_2 = 4*Cf_2*H_2*(G_2**2)/(Dh_2*2*rho_2)  #lead pressure drop

# Task 4.2 - Losses due to the wire wrapper for SFR and spacer grids for LFR. 
# Wire wrap and spacer calcs
Hw = 0.1  #wire lead
M=((1.034)/(PD_1**0.124)+(29.7*(PD_1**6.94)*(np.mean(Re_1)**0.086))/((Hw/Dfuel_1)**2.239))**0.885
dp_fric_1 = M*dp_fric_1

# Spacer grid pressure drop - LFR
eta_2 = 1.95*(np.mean(Re_2)**(-0.08))
dp_spacer_2 = 10*eta_2*(G_2**2)/(2*rho_2)

# Task 4.3 - Elevation pressure drop / Gravity pressure drop
dp_grav_1 = rho_1*9.81*H_1
dp_grav_2 = rho_2*9.81*H_2

# Task 4.4 - Local losses at the assembly inlet and exit.
eta_in  = 0.5
eta_out = 1.0
dp_inlet_1 = eta_in*(G_1**2)/(2*rho_1)
dp_inlet_2 = eta_in*(G_2**2)/(2*rho_2)
dp_outlet_1 = eta_out*(G_1**2)/(2*rho_1)
dp_outlet_2 = eta_out*(G_2**2)/(2*rho_2)

# Task 4.5 - Calculate the total pressure drop across the core.
# Total pressure drop
dp_1 = dp_fric_1 + dp_grav_1 +  dp_inlet_1  + dp_outlet_1
dp_2 = dp_fric_2 + dp_grav_2 +  dp_inlet_2  + dp_outlet_2 + dp_spacer_2
