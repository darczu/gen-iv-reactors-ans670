% Exercise with AI.  
% Copilot AI precise option. AI coupled with Internet Explorer.
% PROMPT:  Generate me matlab code showing how to use iteration to solve implicit equation.
% Bing AI, or ChatGPT may generate sth different.


% Define the function and its derivative
f = @(x) x^3 - x^2 + 2;
df = @(x) 3*x^2 - 2*x;

% Initial guess
x0 = 0.4; 

% Tolerance
tol = 1e-6;

% Maximum number of iterations
max_iter = 10000;

% Newton-Raphson iteration
for i = 1:max_iter
    i
    x1 = x0 - f(x0)/df(x0);
    
    % Check for convergence
    if abs(x1 - x0) < tol
        abs(x1 - x0)
        break
    end
    
    x0 = x1;
end

% Display the root
disp(x1)