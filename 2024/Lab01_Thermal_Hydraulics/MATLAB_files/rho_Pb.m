% Lead density [kg/m3]
function [rho_Pb] = rho_Pb(T_Pb) 
rho_Pb = 11367 - 1.1944.*T_Pb; %Wallenius
end