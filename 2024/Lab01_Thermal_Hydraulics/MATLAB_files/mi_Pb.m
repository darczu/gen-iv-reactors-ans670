%Dynamic Viscosity - Wallenius     Dynamic Visc. = [kg/s/m] = [Pa*s]
function [mi_Pb] = mi_Pb(T_Pb)
mi_Pb = 4.55.*(10^-4).*(exp(1069./T_Pb));              %[kg/m/s]       %[Pa*s] =              
end