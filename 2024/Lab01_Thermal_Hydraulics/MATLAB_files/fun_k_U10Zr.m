%Metallic Fuel U10Zr Thermal Conductivity
function [k] = fun_k_U10Zr(T)

k = 9.620 + 0.0225.*T+(3.27e-6).*(T.^2); %Wallenius Chapter 6

end