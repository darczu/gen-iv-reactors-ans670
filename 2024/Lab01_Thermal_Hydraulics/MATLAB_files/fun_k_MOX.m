%MOX fuel Thermal Conductivity
function [k] = fun_k_MOX(T)

k = (1./(0.042 + 2.71*(10.^-4).*T)) + 6.9*(10.^-11).*(T.^3);  %Fast Spectrum Reactors

%k = (1/(0.0363+T/4011))+(T/2245).^(3)  %Wallenius

end