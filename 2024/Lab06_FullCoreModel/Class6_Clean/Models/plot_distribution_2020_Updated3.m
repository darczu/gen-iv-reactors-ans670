clear; clc; close all;

%run ./RESULTS/SFR_3600_oxide_2D/input2D_det0.m
%run ./2d_refined/input_det0.m


run ./older_res/input3D_with_det_det0.m
run ./older_res/input3D_with_det_core0.m
run ./older_res/input3D_with_det_his0.m

%run ./input3D_with_det_det0.m
%run ./input3D_with_det_core0.m
%run ./input3D_with_det_his0.m


N = 100;
Ngrid = 15;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DETECTOR 3 with Total Fission Distribution
[X,Y] = meshgrid(DET3X(:,2),DET3Y(:,2));   
 RESULT3=DET3(:,11);    %read Detector 3 Results
 RESULT3_2 = reshape(RESULT3,N,N);		 %matrix reshaping to 1x900 to 30x30 matrix
 RESULT3_2=RESULT3_2./(max(max(RESULT3_2)));   %normalization to max value
 
%% different plots are possible
%bar3(RESULT3_2)
%imagesc(RESULT2)
%mesh(RESULT2)
%contourf(RESULT2)
%surf(RESULT2)
%surf(RESULT2)


%% Plot DETECTOR 3 and Normalized fission reaction distribution 
figure(1)
%surf(RESULT3_2)
%b=imagesc(RESULT3_2)
b=bar3(RESULT3_2)
grid on; box on;
title('Normalized Fission Rate - 2D Mesh Detector #3')

% Coloring according to the bar height https://www.mathworks.com/help/matlab/creating_plots/color-3-d-bars-by-height-1.html
for k = 1:length(b)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = 'interp';
end
colorbar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DETECTOR 1 - NEUTRON SPECTRA
figure(2)
semilogx(DET1E(:,2),DET1(:,11),'r-' )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DETECTOR 2 - LATTICE FLUX DETECTOR
%
RESULT2 = DET2(:,11); 
RESULT2_2 = reshape(RESULT2,Ngrid,Ngrid);
RESULT2_2 =RESULT2_2./(max(max(RESULT2_2)));   %normalization to max value
%
figure(3)
imagesc(RESULT2_2)
grid on; box on;
title('Normalized Neutron Flux - Lattice Detector #2')
colorbar;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% READ DETECTOR 4 - Hexagonal mesh detector - Total Fission Reaction Rate
RESULT4 = DET4(:,11); 
RESULT4_2 = reshape(RESULT4,21,21);
%RESULT4_2 = reshape(RESULT4,Ngrid,Ngrid);
RESULT4_2 =RESULT4_2./(max(max(RESULT4_2)));   %normalization to max value

figure(4)
imagesc(RESULT4_2)
grid on; box on;
title('Normalized Fission Rate - 2D Hexagonal Detector #4')
colorbar;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% READ CPD DETECTOR - CORE POWER DISTRIBUTION
power0  = zeros(Ngrid*Ngrid,1);

for i=1:numel(lvl0(:,1)) %
    power0(lvl0(i,1)) = lvl0(i,2);
end

power = reshape(power0,[Ngrid Ngrid])
power1 = power./(max(max(power)))

%% Normalized power distribution
figure(5)
imagesc(power1)
grid on; box on;
title('Normalized Power Dsitribution - CPD pin-wise detector')
colorbar;


%}


