% AI PROMPT: generate me matlab code with simple Monte Carlo method demonstration
% Copilot AI in the Internet Explorer
% Modified by Piotr Darnowski 2024 - added relative difference

clear; clc; close all;
% Number of darts that will be thrown
numDarts = 1e2;
%numDarts = [1e2, 1e3, 1e4, 1e5, 1e6, 1e7]

% Throw darts at the square
darts = rand(numDarts, 2);

% Compute the distance of the darts from the origin
distances = sqrt(sum(darts.^2, 2));

% Find darts that hit the circle
dartsInCircle = distances <= 1.0;

% Estimate pi
estimatedPi = 4 * sum(dartsInCircle) / numDarts;

fprintf('Estimated value of pi: %f\n', estimatedPi);

% Relative Difference [%]
RD = 100*(estimatedPi - pi)./pi