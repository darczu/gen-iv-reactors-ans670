% Script to compare different neutron Spectra
% Task 2.11
clear; close all; clc;
% Read data:


idx = 0;
run SFR/input_res.m;
run SFR/input_det0.m;

% Neutron flux per lethargy:
figure(1);
clf;
semilogx(DET7E(:,3), DET7(:,11),'b-');
xlabel('Neutron energy (MeV)');
ylabel('Flux per lethargy');
title('Neutron flux per lethargy');
grid on;
hold on;
idx = 0;

run LFR/input_res.m;
run LFR/input_det0.m;

semilogx(DET7E(:,3), DET7(:,11),'r-');
axis([1e-6 1e2])

print -dpng 'flux_cmp.png';
