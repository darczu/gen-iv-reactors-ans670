% LAB_03 Task 1   Rev
% PLOT SCRIPT Task 1

% Initial
clear; clc; close all;		% Clear workspace

figure(1); 					%Initialize figure 1
clf;
hold on; 					%holds on the figure.

% Read data
x = [1.1, 1.1897, 1.3, 1.4, 1.5]	%Cases


% Plotting

% P/D = 1.1
idx = 0;  						% Reset data reading index
run PD1.1/input_res;			% run matlab output files to add them to the workspace - CHANGE IT TO BE CONSISTENT WITH YOUR FOLDER
i = 1;                          % Case index
y(i)  = ANA_KEFF(:,1);			% y equals to the Analog k-eff	
dy(i) = ANA_KEFF(:,2);			%dy equal to the Analog k-eff relative statistical error 
% The error estimates in the _res.m file are all relative statistical errors, i.e. standard deviations divided by the mean value
% In Monte Carlo calculation we usually present our results in the following way:
% k_eff +- 2SD - obtained eigenvalue estimator plus-minus two absolute standard deviations
% where SD is relative statisical error times the mean value:
y_2SD(i) = 2.*y(i).*dy(i);


		
% P/D = 1.1897- BASE CASE CALCULATED INITIALLY		
idx = 0;
run PD1.18/input_res;			
i = 2;
y(i)  = ANA_KEFF(:,1);	
dy(i) = ANA_KEFF(:,2);
y_2SD(i) = 2.*y(i).*dy(i);




% P/D = 1.3
idx = 0;
run PD1.3/input_res;
i = 3;
y(i)  = ANA_KEFF(:,1);	
dy(i) = ANA_KEFF(:,2);
y_2SD(i) = 2.*y(i).*dy(i);

% P/D = 1.4
idx = 0;
run PD1.4/input_res;
i = 4;
y(i)  = ANA_KEFF(:,1);	
dy(i) = ANA_KEFF(:,2);
y_2SD(i) = 2.*y(i).*dy(i);


% P/D = 1.5
idx = 0;
run PD1.5/input_res;
i = 5;
y(i)  = ANA_KEFF(:,1);	
dy(i) = ANA_KEFF(:,2);
y_2SD(i) = 2.*y(i).*dy(i);


% Plot
errorbar(x,y,y_2SD,'o-r');


xlabel('Pitch-to-Diameter P/D');            
ylabel('k-eigenvalue with 2SD errorbars')
grid on; 
box on;
xlim([1,1.6]);
ylim([1.3,1.5]);
legend('k-eff vs P/D');
print -dpng 'plot.png';
