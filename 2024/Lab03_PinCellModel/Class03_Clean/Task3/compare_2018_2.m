% Revision 14-05-2018
% Task 3.5
clear; close all; clc;
% Read data:
%Comparison of the fuels


% Read Results
idx = 1;

run oxide/oxide_res.m;
run oxide/oxide_det0.m;

px1=DETAxialFluxZ(:,2);  %axial node position
py1=DETAxialFlux(:,11);  %axial flux

fx1=DETAxialFissionZ(:,2); 	%axial node position
fy1=DETAxialFission(:,11);  %axial fission rate

x1 = DET10E(:,3); %energy bins
y1 = DET10(:,11);  %spectrum values



% Read Results
idx = 1;

run metallic/metallic_res.m;
run metallic/metallic_det0.m;

px2=DETAxialFluxZ(:,2);  %axial node position
py2=DETAxialFlux(:,11);  %axial flux

fx2=DETAxialFissionZ(:,2); 	%axial node position
fy2=DETAxialFission(:,11);  %axial fission rate

x2=DET10E(:,3);  %energy bins
y2=DET10(:,11);  %spectrum




%%
% Neutron flux per lethargy:
figure(1);
clf; hold on; box on; grid on;
xlabel('Energy, [MeV]');
ylabel('Flux per Lethargy, [a.u.]');
semilogx(x1,y1,x2,y2);
legend('oxide', 'metallic');
print -dpng 'spectrum_cmp_fuels.png';


% Normalized neutron flux distributions vs Fission reaction rate
figure(2);
clf; hold on; box on; grid on;
xlabel('Elevation, [cm]');
ylabel('Flux/Power, [a.u]');
grid on;

%plot fluxes
plot(px1,py1./max(py1),'ob',px2,py2./max(py2),'or'); % plot fluxes

%plot fission rate
plot(fx1,fy1./max(fy1),'b--',fx2,fy2./max(fy2),'r--'); % plot fluxes

%plot(px1,py1,'o-',px2,py2,'o-'); %absolute
legend('oxide flux', 'metallic flux', 'oxide fission rate', 'metallic fission rate');
print -dpng 'power_cmp.png';



