clear; clc; close all;

%run ./RESULTS/SFR_3600_oxide_2D/input2D_det0.m
run ./2d/input2D_det0.m


%% Plot detector 3 with Total Fission Distribution
[X,Y] = meshgrid(DET3X(:,2),DET3Y(:,2));   
 RESULT3=DET3(:,11);    %read Detector 3 Results
 RESULT3_2 = reshape(RESULT3,30,30);		 %matrix reshaping to 1x900 to 30x30 matrix
 RESULT3_2=RESULT3_2./(max(max(RESULT3_2)));   %normalization to max value
 


%% different plots are possible
%bar3(RESULT3_2)
%imagesc(RESULT2)
%mesh(RESULT2)
%contourf(RESULT2)
%surf(RESULT2)
%surf(RESULT2)


%% Normalized fission reaction distribution - DETECTOR 3
figure(1)
%surf(RESULT3_2)

b=bar3(RESULT3_2)
grid on; box on;
title('Normalized Fission Reaction Rate')

% Coloring according to the bar height https://www.mathworks.com/help/matlab/creating_plots/color-3-d-bars-by-height-1.html
for k = 1:length(b)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = 'interp';
end
colorbar;


%{

%% DETECTOR 2 - Neutron Flux
%
RESULT2 = DET2(:,11); 
RESULT2_2 = reshape(RESULT2,21,21);
RESULT2_2 =RESULT2_2./(max(max(RESULT2_2)));   %normalization to max value
%
figure(2)
surf(RESULT2_2)
grid on; box on;
title('Normalized Neutron Flux')
colorbar;


%% READ CORE CPD RESULTS
run ./RESULTS/SFR_3600_oxide_2D/input2D_core0.m

power0  = zeros(21*21,1);

for i=1:numel(lvl0(:,1)) %
    power0(lvl0(i,1)) = lvl0(i,2);
end

power = reshape(power0,[21 21])
power1 = power./(max(max(power)))


%% Normalized power distribution
figure(3)
surf(power1)
grid on; box on;
title('Normalized Power Dsitribution')
colorbar;

%}