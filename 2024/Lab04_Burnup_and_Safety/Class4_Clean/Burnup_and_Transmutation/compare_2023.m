clear; close all; clc;
% Read data:
% Comparison of the fuels for bunrup and transmutation tutorial

%% ADS  ===============================================================================================
% ADS
idx = 0;
run ADS/ADS_res.m;  								% read results
run ADS/ADS_dep.m;  								% read results

name1= NAMES;			
power1 = TOT_POWER(1,1);  							% [W] Power
b1 = BURNUP(:,1);         							% [GWd/tHM] = [kWd/gHM]  bunrup
d1 = BURN_DAYS;           							% [days]  burnup time
k1 = ANA_KEFF(:,1);       							% [-] effective multiuplication factor
rho1 = 1e5.*(k1-1)./k1;	  							% [pcm] reactivity	
Vol1= 47.78362426;		  							% [cm3] volume
%12580.3
EO1 = DAYS(end)*24*power1*(1e-12); 					%[TWh] Energy output
twh1 = (TOT_MASS(:,end)-TOT_MASS(:,1))./EO1./1000	%[kg/TWh]  Bunrup rate per generated energy 

%% SFR with 0% Minor Actinides =========================================================================
% SFR MA0
idx = 0;
run SFR_MA0/SFR_MA0_res.m;
run SFR_MA0/SFR_MA0_dep.m;

name2= NAMES;
power2 =TOT_POWER(1,1); % [kW/gHM]
b2 = BURNUP(:,1);             % [GWd/tHM] = [kWd/gHM]
d2 = BURN_DAYS;         %[days]
k2 = ANA_KEFF(:,1);
rho2 = 1e5.*(k2-1)./k2;
Vol2 = 71.04346718;
EO2 = DAYS(end)*24*power2*(1e-12); %[TWh] Energy output
twh2 = (TOT_MASS(:,end)-TOT_MASS(:,1))./EO2./1000

%% SFR with 5% Minor Actinides =========================================================================
%SFR MA5
idx = 0;
run SFR_MA5/SFR_MA5_res.m;
run SFR_MA5/SFR_MA5_dep.m;

name3= NAMES;
power3 = TOT_POWER(1,1); 
b3 = BURNUP(:,1);
d3 = BURN_DAYS;
k3 = ANA_KEFF(:,1);
rho3 = 1e5.*(k3-1)./k3;
Vol3 = 71.04346718;
EO3 = DAYS(end)*24*power3*(1e-12); %[TWh] Energy output
twh3 = (TOT_MASS(:,end)-TOT_MASS(:,1))./EO3./1000

%% POST PROCESSING RESULTS===============================================================================
figure(1);
hold on; box on; grid on;
xlabel('Burnup, [GWd/tHM]');
ylabel('Eff. Multi. Factor, [-]');
plot(b1,k1,'-ob',b2,k2,'-r*',b3,k3,'-<g');
%plot(b4,k4,'-co');
legend('ADS', 'FR MA0', 'FR MA5'); %,'ADS KTH');
print -dpng 'keff.png';

figure(2);
hold on; box on; grid on;
%xlabel('Burnup, [GWd/tHM]');
xlabel('Time, [EFPD]');
ylabel('Reactivity swing, [pcm]');
%plot(b1,rho1,'-ob',b2,rho2,'-r*',b3,rho3,'-.g');
plot(b1,rho1,'-ob',b2,rho2,'-r*',b3,rho3,'-<g'); %  ,b4,rho4,'-co');
%plot(d1,rho1,'-ob',d2,rho2,'-r*',d3,rho3,'-.g',d4,rho4,'-co');
legend('ADS Pb', 'SFR MA0', 'SFR MA5');   %','ADS PbBi KTH');
print -dpng 'rho.png';


%% =======================================================================================================
%  %{ ARTIFACT
%  % TEST ADS KTH
%  idx = 1;
%  run Task2and3/ADS_KTH/input_res.m;
%  run Task2and3/ADS_KTH/input_dep.m;
%  b4 = BURNUP;
%  d4 = BURN_DAYS;
%  k4 = ANA_KEFF(:,1);
%  rho4 = 1e5.*(k4-1)./k4;
%  Vol4 = 6.082120E+01;
%  EO4 = DAYS(end)*24*17921.2*(1e-12); %[TWh] Energy output
%  twh4 = 100.*(TOT_MASS(:,end)-TOT_MASS(:,1))./EO4./1000
%  %}

%% =======================================================================================================

%TOT_MASS(:,end)-TOT_MASS(:,1)
%semilogy(DAYS,TOT_MASS)
%  twh1 = (TOT_MASS(:,end)-TOT_MASS(:,1))./EO./1000

